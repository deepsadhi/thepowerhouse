<?php

header('content-type: application/json; charset=utf-8');

$title = 'Monthly Average Temperature';
$subtitle = 'Source: WorldClimate.com';
$xAxis = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun','Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
$yaxis = 'Temperature (°C)';
$valueSuffix = '°C';


$series = array();
array_push($series, array('name'=> 'Tokyo', 'data'=> array(7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6)));
array_push($series, array('name'=> 'New York', 'data'=> array(-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5)));
array_push($series, array('name'=> 'London', 'data'=> array(3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8)));

$data = array('title'=>$title, 'subtitle'=>$subtitle, 'xAxis'=>$xAxis, 'yaxis'=>$yaxis, 'valueSuffix'=>$valueSuffix, 'series'=>$series);

echo json_encode($data);
