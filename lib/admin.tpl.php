<?php

$admin_page_template = '

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>{{title}}</title>
</head>
<body>
<section>
	<header>
		<nav>
			{{{navigation}}}
		</nav>
	<h1>LiB<h1>
	</header>
<hr>
</section>
{{{body}}}
<footer>
	<br>
	<hr>
	Copyleft :)
</footer>
</body>
</html>

';
