<?php

/**
 * databases constants
 * define mysql hostname, username, password and table name here
 */

defined('DB_HOST') ? null : define('DB_HOST', 'localhost');
defined('DB_USER') ? null : define('DB_USER', 'web');
defined('DB_PASS') ? null : define('DB_PASS', 'logweb');
defined('DB_NAME') ? null : define('DB_NAME', 'thepowerhouse');
