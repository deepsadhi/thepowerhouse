<?php

header('content-type: application/json; charset=utf-8');
require_once('init.inc.php');

function genJSON($table, $xaxis, $yaxis_arr, $yaxis_title, $name,$title='title', $subtitle='subtitle', $valueSuffix=''){
	global $database;
	
	$sql = "SELECT {$xaxis} FROM $table";
	$database->sql($sql);
	$res = $database->getResult();
	$xaxis_array = array();
	foreach($res as $op){
		array_push($xaxis_array, $op[$xaxis]);
	}

	$series = array();
	$i=0;
	foreach($yaxis_arr as $yaxis){
		$sql = "SELECT {$yaxis} FROM $table";
		$database->sql($sql);
		$res = $database->getResult();
		$data = array();
		foreach($res as $op){
			array_push($data, intval($op[$yaxis]));
		}

		$series_data = array('name'=>$name[$i++], 'data'=>$data);
		array_push($series, $series_data);
	}


	$JSONdata = array('title'=>$title, 'subtitle'=>$subtitle, 'xAxis'=>$xaxis_array, 'yaxis'=>$yaxis_title, 'valueSuffix'=>$valueSuffix, 'series'=>$series);
	

echo json_encode($JSONdata);
}

$id = $_GET['id'];
if($id ==1){
genJSON('plans', 'year', array('energy_gw', 'peak_load_mw'), 'Energy (GW)', array('Energy GW', 'Peak Load MW'), 'Load Forecast', 'Source: NEA Annual Report 2012');
}else if ($id == 2){
	genJSON('loadshedding', 'date_bs', array('current'), 'Hours/Week', array('Current LS'), 'Load Shedding', 'Source: News articles');
}
