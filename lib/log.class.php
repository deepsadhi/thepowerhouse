<?php

require_once(LIB_PATH.DS.'functions.inc.php');

class Log
{
	
	static private $log_path = LOG_PATH;
	static private $log_file = LOG_FILE;
	static private $dir_permission = '0755';
	static private $file_permission = 0755;
	static private $new_log_file; 

	static public function log_action($action, $message=""){
		self::$new_log_file = file_exists(self::$log_file);
		if(check_permissions(self::$log_path) == self::$dir_permission){
			if($handle = fopen(self::$log_file, 'a')){
				$timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
				$content = "{$timestamp} | {$action}: {$message}\n";
				fwrite($handle, $content);
				fclose($handle);
				if(self::$new_log_file){
					chmod(self::$log_file, self::$file_permission);
				}
			}else{
				die("Warning! could not open log file for writing");
			}
		}else{
			die("Warning! could not access logs directory");
		}
	}
}



